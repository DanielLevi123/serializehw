﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace CarH.W
{
    class Program
    {
        static void Main(string[] args)
        {
            Car renualt = new Car("Clio", "Renualt", 2004, "Grey",1234, 5);
            XmlSerializer myXmlSerializer = new XmlSerializer(typeof(Car));
            using (Stream file = new FileStream(@"xmlfile.xml", FileMode.Create))
            {
                myXmlSerializer.Serialize(file, renualt);
            }
            Car c;
            using (Stream file = new FileStream(@"xmlfile.xml", FileMode.Open))
            {
              c = myXmlSerializer.Deserialize(file) as Car;
            }
            Console.WriteLine(c);
            Car[] arrayOfCars = new Car[5];
            arrayOfCars[0] = new Car("Swift", "Suzuki", 2019, "Red", 1111, 5);
            arrayOfCars[1] = new Car("Alto", "Suzuki", 2014, "White", 1123, 4);
            arrayOfCars[2] = new Car("Octavia", "Skoda", 2019, "Pearl", 1311, 5);
            arrayOfCars[3] = new Car("Superb", "Skoda", 2007, "Blue", 3111, 5);
            arrayOfCars[4] = new Car("308", "Peugeot", 2020, "Black", 2311, 5);
            XmlSerializer cararrayserializer = new XmlSerializer(typeof(Car[]));
            using (Stream file= new FileStream(@"carxmlfile.xml", FileMode.Create))
            {
                cararrayserializer.Serialize(file, arrayOfCars);
            }
            Car[] cars;
            using (Stream file = new FileStream(@"carxmlfile.xml", FileMode.Open))
            {
                cars = cararrayserializer.Deserialize(file) as Car[];
            }
            foreach (Car c1 in cars)
            {
                Console.WriteLine(c1);
            }
            Console.WriteLine("==================================");
            Car automobile = new Car("xmlfile.xml");
            Console.WriteLine(automobile);
            renualt.CarCompare("xmlfile.xml");
        }
    }
}
