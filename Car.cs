﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace CarH.W
{
    public class Car
    {
        public string Model { get; set; }
        public string Brand { get; set; }
        public int Year { get; set; }
        public string Color { get; set; }
        private int codan = 1234;
        protected int numberOfSeats = 5;
        public Car()
        {

        }
        public Car(string fileName)
        {
            Car carFromFile = Car.DeserializeACar(fileName);
            this.Model = carFromFile.Model;
            this.Brand = carFromFile.Brand;
            this.Year = carFromFile.Year;
            this.Color = carFromFile.Color;
        }

        public Car(string model, string brand, int year, string color, int codan, int numberOfSeats)
        {
            Model = model;
            Brand = brand;
            Year = year;
            Color = color;
            this.codan = codan;
            this.numberOfSeats = numberOfSeats;
        }
        public int GetCodan()
        {
            return this.codan;
        }
        public int GetNumberOfSeats()
        {
            return this.numberOfSeats;
        }
        protected void ChangeNumberOfSeats(int newNumberOFSeats)
        {
            if(newNumberOFSeats>0)
            {
                this.numberOfSeats = newNumberOFSeats;
            }
        }
        public static void SerializeACar(string fileName,Car car)
        {
            XmlSerializer myXmlSerializer = new XmlSerializer(typeof(Car));
            using (Stream file = new FileStream(fileName, FileMode.Create))
            {
                myXmlSerializer.Serialize(file ,car);
            }

        }
        public static void SerializeCarArray(string fileName,Car[] cars)
        {
            XmlSerializer myXmlSerializer = new XmlSerializer(typeof(Car));
            using (Stream file = new FileStream(fileName, FileMode.Create))
            {
                myXmlSerializer.Serialize(file, cars);
            }
        }
        public static Car DeserializeACar(string fileName)
        {
            XmlSerializer myXmlSerializer = new XmlSerializer(typeof(Car));
            Car c=null;
            using (Stream file = new FileStream(fileName, FileMode.Open))
            {
                c = myXmlSerializer.Deserialize(file) as Car;
            }
            return c;
        }
        public static Car[] DeserializeCarArray(string fileName)
        {
            XmlSerializer myXmlSerializer = new XmlSerializer(typeof(Car));
            Car[] cars=null;
            using (Stream file = new FileStream(fileName, FileMode.Open))
            {
                cars = myXmlSerializer.Deserialize(file) as Car[];
            }
            return cars;
        }
        public bool CarCompare(string fileName)
        {
            Car carFromFile = Car.DeserializeACar(fileName);
            if (this.Model == carFromFile.Model &&
                this.Brand == carFromFile.Brand &&
                this.Year == carFromFile.Year &&
                this.Color == carFromFile.Color)
            {
                return true;
            }
            return false;
        }
        public override string ToString()
        {
            return $"[{base.ToString()}] {Model} {Brand} {Year} {Color} {codan} {numberOfSeats}";
        }
    }
}
